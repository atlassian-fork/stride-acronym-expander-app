// init project
const express = require("express");
const app = express();
const bodyParser = require("body-parser");

//The Stride JS client library handles API authentication and calling REST endpoints from the Stride API
const { CLIENT_SECRET } = process.env;
const stride = require("./strideClient");

//Middleware
const auth = require("./middleware/auth")(CLIENT_SECRET);
const tutorialMiddleware = require("./middleware/tutorialEndpoints");
const lifecycle = require("./middleware/lifecycle");

let botUser = null;

//Caches the bot user info for use when parsing direct messages
async function getBotsUser() {
  if (!botUser) {
    botUser = await stride.api.users.me().catch(err => {
      console.error(`Bot user lookup failed.`);
      throw err;
    });
  }
  return botUser;
}

app.use(bodyParser.json());
app.use(tutorialMiddleware);
app.use(lifecycle);


app.post(
  "/mentions",
  auth,
  async (req, res, next) => {
    //for webhooks send the response asap or the messages will get replayed up to 3 times
    res.sendStatus(204);
    next();
  },
  showMessageCommands
);

app.post(
  "/direct",
  auth,
  async (req, res, next) => {
    //for webhooks send the response asap or the messages will get replayed up to 3 times
    res.sendStatus(204);

    let mentions = stride.utils.getMentionIdsFromADF(req.body.message);

    if (mentions.includes((await getBotsUser()).account_id)) {
      console.log("direct message received with @ mention. Ignoring to prevent duplicate responses");
      return;
    }

    next();
  },
  showMessageCommands
);

async function showMessageCommands(req, res, next) {
  console.log("sending message with actions");

  //The auth middleware stores the decrypted JWT token values as res.locals.context
  //The cloudId and conversationId are needed to send a message back
  const { cloudId, conversationId } = res.locals.context;
  await require("./messages/messageWithActions")(cloudId, conversationId);
}


// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log("Your app is listening on port " + listener.address().port);
});
