const stride = require("../strideClient");
const { Document } = require("adf-builder");

function createMessage(message, userId) {
  let doc = new Document();
  let paragraph = doc.paragraph();

  if (userId) {
    paragraph.mention(userId, "").text(" says : ");

    doc
      .blockQuote()
      .paragraph()
      .text(message);
  } else {
    paragraph.text("The external page submitted : ");
    doc
      .blockQuote()
      .paragraph()
      .text(message);
  }
  return { body: doc.toJSON() };
}

module.exports = async function(cloudId, conversationId, message, userId) {
  //send responses to the conversation
  await stride.api.messages.sendMessage(cloudId, conversationId, createMessage(message, userId));
};
